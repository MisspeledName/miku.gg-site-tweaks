# miku.gg - Site Tweaks
This userstyle makes some quality of life improvements to the [miku.gg](https://miku.gg/) site.

<img src="novel%20fullscreen.webp"/>

## Features
- Clean a bit of clutter from novel layout including the faded border around the edges.
- Set a custom height for the way too short input and memory boxes in novels.
- Override a variety of settings for the chatbox in novels. Among other benefits this fixes the problem of the site not remembering sizing and placement for the chatbox.
- Expand novels to take up the full page instead of the tiny portion they default into.
- Layout improvements to make the Saved Narrations page more usable.
- A configuration menu where all of these options can be adjusted to your liking.

## Installing
You'll need a userstyle manager that supports the stylus preprocessor such as the Stylus browser extension.

You can find info on how to install Stylus here.
<br>https://github.com/openstyles/stylus

From there I recommend installing the userstyle from my userstyles.world page.
<br>https://userstyles.world/style/16673/miku-gg-site-tweaks

## Notes
Most of the features default to disabled so be sure to take a look at the configure menu and enable the ones you prefer.

Sometimes turning off the override chatbox settings feature makes the width of part of the chatbox temporarily bug out.
<br>Reloading the page or using the site's move tool on the chatbox fixes it.
